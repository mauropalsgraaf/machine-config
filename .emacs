(setq backup-directory-alist `(("." . "~/.saves")))

(require 'package)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (helm-spotify markdown-mode sublime-themes ensime use-package haskell-mode ##))))
(package-initialize)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil)))))

(global-linum-mode t)

(require 'package)
(add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '(("melpa" . "http://melpa.org/packages/") ; many packages won't show if using stable
   ("melpa" . "http://melpa.org/packages/"))
   t))

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

(load-theme 'spolsky t)
