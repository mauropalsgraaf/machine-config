# Machine-config

Move all the files to the correct folder.

## Emacs

By default, the emacs version on OSX is 22.x which is not suitable for the config file. Install a newer version with `brew install emacs`. Make sure to recheck the path in the `.zshrc` file. Then, make sure to get some packages. Go into emacs and do the following command: `M-x package-refresh-contents` and then `M-x package-install RET sublime-themes`
