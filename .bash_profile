[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
export CLASSPATH=".:/usr/local/Cellar/antlr/4.5.1/antlr-4.5.1-complete.jar:$CLASSPATH"
export ANDROID_HOME="/usr/local/Cellar/android-sdk/24.3.4/"
export GRADLE_HOME="/usr/local/bin/gradle/"

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_121.jdk/Contents/Home 
export PATH=$JAVA_HOME/bin:$PATH 

ulimit -n 2560

# add homebrew php to path
# export PATH="$(brew --prefix homebrew/php/php56)/bin:$PATH"

# show git status at my prompt
source ~/git-prompt.sh
GIT_PS1_SHOWUPSTREAM="auto"
GIT_PS1_SHOWCOLORHINTS="yes"
PROMPT_COMMAND='__git_ps1 "[\u@\h:\W"] "\\\$ "'

# vagrant
# export VAGRANT_DEFAULT_PROVIDER=vmware_fusion

# Set history to unlimited
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "

# Set max open files 1024
ulimit -n 1024

# Git editor
export GIT_EDITOR=vim

export PATH="$HOME/.cargo/bin:$PATH"
